<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'width',
        'height',
        'position_top',
        'position_left'
    ];

    /**
     * Default values for attributes
     */
    protected $attributes = [
        'width' => 0,
        'height' => 0,
        'position_top' => 0,
        'position_left' => 0,
    ];

    /**
     * Get the plants for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function plants()
    {
        return $this->hasMany('App\Models\Plant', 'zone_id', 'id');
    }


    /**
     * Get the tasks for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'zone_id', 'id');
    }
}
