<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'plant_definition_id',
        'cultivar',
        'zone_id'
    ];

    /**
     * Get the tasks for this model.
     *
     * @return App\Models\Task
     */
    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'plant_id');
    }

    public function plant_definition(){
        return $this->belongsTo('App\Models\PlantDefinition', 'plant_definition_id');
    }

    public function zone(){
        return $this->belongsTo('App\Models\Zone', 'zone_id');
    }
}
