<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PlantDefinition;
use App\Http\Requests\Api\StorePlantDefinitionRequest;
use App\Http\Requests\Api\UpdatePlantDefinitionRequest;
use App\Http\Resources\PlantDefinitionCollection;
use App\Http\Resources\PlantDefinitionResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;


class PlantDefinitionApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/plant-definition",
     *      operationId="listPlantDefinitions",
     *      tags={"PlantDefinition"},
     *      summary="List all plants definitions",
     *      description="Returns plant definitions data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        //abort_if(Gate::denies('project_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        Log::debug("Listing the plant definitions");
        return new PlantDefinitionCollection(PlantDefinition::paginate());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/plant-definition",
     *      operationId="storePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Store new plant definition",
     *      description="Returns plant definition data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "family"},
     *              @OA\Property(property="name", type="string", example="Tomate"),
     *              @OA\Property(property="family", type="string", example="Solanaceae"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StorePlantDefinitionRequest $request)
    {
        $plant_definition = PlantDefinition::create($request->all());

        return (new PlantDefinitionResource($plant_definition))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="getPlantDefinitionById",
     *      tags={"PlantDefinition"},
     *      summary="Get plant definition information",
     *      description="Returns plant definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "name": "Tomate", 
     *                      "family": "Solanaceae",
     *                      "description": "..."
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show plant definition $id");
        // abort_if(Gate::denies('project_show'),   Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_definition = PlantDefinition::findOrFail($id);
        return (new PlantDefinitionResource($plant_definition))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="updatePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Update existing plant definition",
     *      description="Returns updated plant definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "family"},
     *              @OA\Property(property="name", type="string", example="Tomate"),
     *              @OA\Property(property="family", type="string", example="Solanaceae"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdatePlantDefinitionRequest $request)
    {
        Log::debug("Udpate plant definition $id");
        $plant_definition = PlantDefinition::findOrFail($id);
        $plant_definition->update($request->all());

        return (new PlantDefinitionResource($plant_definition))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="deletePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Delete existing plant definition",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant Definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        // abort_if(Gate::denies('project_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_definition = PlantDefinition::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($plant_definition)) {
            $plant_definition->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}
