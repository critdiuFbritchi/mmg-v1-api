<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StorePlantRequest;
use App\Http\Requests\Api\UpdatePlantRequest;
use App\Http\Resources\PlantResource;
use App\Http\Resources\PlantCollection;
use App\Models\Plant;
use App\Models\PlantDefinition;
use App\Models\Zone;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;


class PlantApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/plant",
     *      operationId="listPlant",
     *      tags={"Plant"},
     *      summary="List all plants",
     *      description="Returns plants data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        //abort_if(Gate::denies('project_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        Log::debug("Listing the plants");
        return new PlantCollection(Plant::paginate());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/plant",
     *      operationId="storePlant",
     *      tags={"Plant"},
     *      summary="Store new plant",
     *      description="Returns plant data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"plant_definition"},
     *              @OA\Property(property="name", type="string", example="Tomate - graines données par paul"),
     *              @OA\Property(property="cultivar", type="string", example="Green Zebra"),
     *              @OA\Property(property="description", type="string", example="..."),
     *              @OA\Property(property="plant_definition", type="integer", example="1"),
     *              @OA\Property(property="zone", type="integer", example="1")
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StorePlantRequest $request)
    {
        $plant = Plant::create($request->all());

        // Set plant definition
        $plant_definition = PlantDefinition::find($request->plant_definition);
        if(!empty($plant_definition)){
            $plant->plant_definition_id = $plant_definition->id;
        }
        // Set zone
        $zone = Zone::find($request->zone);
        if(!empty($zone)){
            $plant->zone_id = $zone->id;
        }
        return (new PlantResource($plant))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/plant/{id}",
     *      operationId="getPlantById",
     *      tags={"Plant"},
     *      summary="Get plant information",
     *      description="Returns plant data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "name": "Tomate", 
     *                      "cultivar": "Green Zebra",
     *                      "description": "...",
     *                      "plant_definition": "{id:1, name:'Pommier', family:'Rosaceae', description:'...'}",
     *                      "zone": "{id:1, name:'zone 1'}",
     *                      "tasks": "[]"
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show plant $id");
        // abort_if(Gate::denies('project_show'),   Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant = Plant::findOrFail($id);
        $plant->load('tasks');
        return (new PlantResource($plant))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/plant/{id}",
     *      operationId="updatePlant",
     *      tags={"Plant"},
     *      summary="Update existing plant",
     *      description="Returns updated plant data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string", example="Tomate - graines données par paul"),
     *              @OA\Property(property="cultivar", type="string", example="Green Zebra"),
     *              @OA\Property(property="description", type="string", example="..."),
     *              @OA\Property(property="plant_definition", type="integer", example="1"),
     *              @OA\Property(property="zone", type="integer", example="1")
     *          ),
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdatePlantRequest $request)
    {
        Log::debug("Udpate plant $id");
        $plant = Plant::findOrFail($id);
        $plant->update($request->all());

        // Set plant definition
        $plant_definition = PlantDefinition::find($request->plant_definition);
        if(!empty($plant_definition)){
            $plant->plant_definition_id = $plant_definition->id;
        }

        // Set zone
        $zone = Zone::find($request->zone);
        if(!empty($zone)){
            $plant->zone_id = $zone->id;
        }

        return (new PlantResource($plant))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/plant/{id}",
     *      operationId="deletePlant",
     *      tags={"Plant"},
     *      summary="Delete existing plant",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        // abort_if(Gate::denies('project_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant = Plant::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($plant)) {
            $plant->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}
