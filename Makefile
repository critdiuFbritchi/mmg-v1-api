USER=$$(whoami)

ifeq ($(OS),Windows_NT)
	detected_OS=Windows
else
	detected_OS=$(shell uname -s)
endif

test_user:
	@echo $(USER)

test_os:
ifeq ($(detected_OS),Windows)
	@echo WINDOWS
else ifeq ($(detected_OS),Linux)
	@echo LINUX
else
	@echo "$(detected_OS)"
endif

install1:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Installing Composer dependencies... && (composer install && echo Composer dependencies installed successfully.) || echo Failed to install Composer dependencies.
	@echo Installing npm dependencies... && (npm i && echo Npm dependencies installed successfully.) || echo Failed to install npm dependencies.
	@echo Copying .env file if necessary... && (if not exist .env copy .env.example .env && echo .env file copied successfully.) || echo Skipping .env copy.
	@echo Running Laravel migrations... && (php artisan migrate && echo Laravel migrations run successfully.) || echo Failed to run Laravel migrations.
	@echo Running BackPack... && (php artisan backpack:install && echo BackPack migrations run successfully.) || echo Failed to run BackPack migrations.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Update...." && (sudo apt update && echo "\033[1;32m✔Update successfully.\033[0m" ) || echo "\033[1;31m✖Failed to update\033[0m"
	@echo "Upgrade...." && (sudo apt upgrade -y && echo "\033[1;32m✔Upgrade successfully.\033[0m" ) || echo "\033[1;31m✖Failed to upgrade\033[0m"
	@echo "Installing apt-utils...." && sudo apt install apt-utils -y && echo "\033[1;32m✔Successfully.\033[0m" || echo "\033[1;31m✖Failed to installed apt-utils\033[0m"
	@echo "Installing PHP...." && sudo apt-get install -y ca-certificates apt-transport-https software-properties-common wget curl lsb-release && (curl -sSL https://packages.sury.org/php/README.txt | sudo bash -x) && sudo apt update -y && (sudo apt install php8.1 -y && echo "\033[1;32m✔PHP installed successfully.\033[0m" ) || echo "\033[1;31m✖Failed to install PHP\033[0m"
	@echo "Installing Docker...." && sudo apt-get install docker -y && echo "\033[1;32m✔Docker installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install Docker\033[0m"
	@echo "Installing NPM...." && sudo apt-get install npm -y && echo "\033[1;32m✔NPM installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install NPM\033[0m"
    # @echo "Add USER to Docker group...." && sudo usermod -aG docker $(USER) && sudo su $(USER) && echo "\033[1;32m✔successfully.\033[0m" || echo "\033[1;31m✖Failed to add USER to docker group\033[0m"
install2:
	@echo "Installing Docker-Compose...." && sudo apt install -y docker-compose && echo "\033[1;32m✔Docker-Compose installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install Docker-Compose\033[0m"
	@echo "Installing Composer...." && (php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer && sudo chmod +x /usr/local/bin/composer && echo "\033[1;32m✔Composer installed successfully.\033[0m") || echo "\033[1;31m✖Failed to install Composer\033[0m"
	@echo "Installing PHP mbstring...." && sudo apt install -y php-mbstring && echo "\033[1;32m✔PHP-mbstring installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install PHP-mbstring\033[0m"
	@echo "Installing PHP Dom...." && sudo apt install php8.1-XML -y && echo "\033[1;32m✔PHP-Dom installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install PHP-Dom\033[0m"
	@echo "Installing PHP Curl...." && sudo apt install php8.1-curl -y  && echo "\033[1;32m✔PHP-Curl installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install PHP-Curl\033[0m"
	@echo "Installing PHP Zip...." && sudo apt install php8.1-zip -y && echo "\033[1;32m✔PHP-Zip installed successfully.\033[0m" || echo "\033[1;31m✖Failed to install PHP-Zip\033[0m"
	@echo "Update Composer...." && composer update && echo "\033[1;32m✔Successfully.\033[0m" || echo "\033[1;31m✖Failed to update Composer \033[0m"
	@echo "Installing NodeJS...." && (curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - && sudo apt-get install -y nodejs && echo "\033[1;32m✔NodeJS installed successfully.\033[0m") || echo "\033[1;31m✖Failed to install NodeJS\033[0m"
	@echo "Installing Composer dependencies..." && (composer install && echo "\033[1;32m✔Composer dependencies installed successfully.\033[0m") || echo "\033[1;31m✖Failed to install Composer dependencies\033[0m"
	@echo "Installing npm dependencies..." && (npm i && echo "\033[1;32m✔Npm dependencies installed successfully.\033[0m") || echo "\033[1;31m✖Failed to install npm dependencies\033[0m"
	@echo "Copying .env file if necessary..." && ([ ! -f .env ] && cp .env.example .env && echo "\033[1;32m✔.env file copied successfully.\033[0m") || echo "\033[1;33mSkipping .env copy\033[0m"
install3:
	@echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
	@echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"
	@echo "Starting container..." && docker-compose up -d || echo "\033[1;32m✔Succeeded\033[0m"
	@echo "Running Laravel migrations..." && ./vendor/bin/sail artisan migrate && echo "\033[1;32m✔Laravel migrations run successfully.\033[0m" || echo "\033[1;31m✖Failed to run Laravel migrations\033[0m"
	# @echo "Running BackPack..." && (php artisan backpack:install && echo "\033[1;32m✔BackPack migrations run successfully.\033[0m") || echo "\033[1;31m✖Failed to run BackPack migrations\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif

up:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Stopping MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampmysqld64 && echo MySQL stopped.) || echo MySQL was already stopped.
	@echo Stopping Apache... && (sc queryex wampapache64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampapache64 && echo Apache stopped.) || echo Apache was already stopped.
	@echo Starting Docker containers... && docker-compose up

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
	@echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"
	@echo "Starting Docker containers..." && ./vendor/bin/sail up

else
	@echo "OS $(detected_OS) not supported"
endif


down:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Stopping Docker containers... && docker-compose down && echo Succeeded. || echo Failed.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo Stopping Docker containers... && docker-compose down && echo "\033[1;32m✔Succeeded\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif


dev:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Stopping MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampmysqld64 && echo MySQL stopped.) || echo MySQL was already stopped.
	@echo Stopping Apache... && (sc queryex wampapache64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampapache64 && echo Apache stopped.) || echo Apache was already stopped.
	@echo Starting container... && docker-compose up -d || echo Succeeded.
	@echo Compiling assets... && npm run dev || echo Failed to compile.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
	@echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"
	@echo "Starting container..." && docker-compose up -d || echo "\033[1;32m✔Succeeded\033[0m"
	@echo "Compiling assets..." && npm run dev || echo "\033[1;31m✖Failed to compile\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif

build:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Stopping MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampmysqld64 && echo MySQL stopped.) || echo MySQL was already stopped.
	@echo Stopping Apache... && (sc queryex wampapache64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampapache64 && echo Apache stopped.) || echo Apache was already stopped.
	@echo Starting container... && docker-compose up -d || echo Succeeded.
	@echo Compiling assets... && npm run build || echo Failed to compile.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
	@echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"
	@echo "Starting container..." && docker-compose up -d || echo "\033[1;32m✔Succeeded\033[0m"
	@echo "Compiling assets..." && npm run build || echo "\033[1;31m✖Failed to compile\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif



service_stop:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Stopping MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampmysqld64 && echo MySQL stopped.) || echo MySQL was already stopped.
	@echo Stopping Apache... && (sc queryex wampapache64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampapache64 && echo Apache stopped.) || echo Apache was already stopped.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
	@echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif

service_start:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Starting MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "STOPPED" >nul && sc start wampmysqld64 && echo MySQL started.) || echo MySQL is already running.
	@echo Starting Apache... && (sc queryex wampapache64 | find "STATE" | find /i "STOPPED" >nul && sc start wampapache64 && echo Apache started.) || echo Apache is already running.

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Starting MySQL..." && (systemctl list-units --type service --all | grep -q 'mysql.service' && sudo systemctl start mysql && echo "\033[1;32m✔MySQL started successfully\033[0m") || echo "\033[1;32mMySQL does not exist\033[0m"
	@echo "Starting Apache..." && (systemctl list-units --type service --all | grep -q 'apache2.service' && sudo systemctl start apache2 && echo "\033[1;32m✔Apache started successfully\033[0m") || echo "\033[1;32mApache does not exist\033[0m"

else
	@echo "OS $(detected_OS) not supported"
endif

clear_cache:
ifeq ($(detected_OS),Windows)
	@echo Running Commands for Windows...
	@echo Clearing Laravel cache... && docker-compose exec -t app php artisan cache:clear && echo Cache cleared successfully! || (echo Failed to clear cache. && exit 1)
	@echo Clearing view cache... && docker-compose exec -t app php artisan view:clear && echo View cache cleared successfully! || (echo Failed to clear view cache. && exit 1)
	@echo Clearing config cache... && docker-compose exec -t app php artisan config:clear && echo Config cache cleared successfully! || (echo Failed to clear config cache. && exit 1)
	@echo Clearing route cache... && docker-compose exec -t app php artisan route:clear && echo Route cache cleared successfully! || (echo Failed to clear route cache. && exit 1)

else ifeq ($(detected_OS),Linux)
	@echo "Running Commands for Linux..."
	@echo "Clearing Laravel cache..." && docker-compose exec -t app "php artisan cache:clear" && echo "\033[1;32m✔ Cache cleared successfully!\033[0m" || (echo "\033[1;31m✖ Failed to clear cache.\033[0m" && true)
	@echo "Clearing view cache..." && docker-compose exec -t app "php artisan view:clear" && echo "\033[1;32m✔ View cache cleared successfully!\033[0m" || (echo "\033[1;31m✖ Failed to clear view cache.\033[0m" && true)
	@echo "Clearing config cache..." && docker-compose exec -t app "php artisan config:clear" && echo "\033[1;32m✔ Config cache cleared successfully!\033[0m" || (echo "\033[1;31m✖ Failed to clear config cache.\033[0m" && true)
	@echo "Clearing route cache..." && docker-compose exec -t app "php artisan route:clear" && echo "\033[1;32m✔ Route cache cleared successfully!\033[0m" || (echo "\033[1;31m✖ Failed to clear route cache.\033[0m" && true)

else
	@echo "OS $(detected_OS) not supported"
endif

bash_laravel:
	@echo "Terminal Laravel:" && docker-compose exec laravel.test bash

bash_mailpit:
	@echo "Terminal mailpit:" && docker-compose exec mailpit sh

bash_meilisearch:
	@echo "Terminal Meilisearch:" && docker-compose exec meilisearch sh

bash_selenium:
	@echo "Terminal Selenium:" && docker-compose exec selenium sh

bash_mysql:
	@echo "Terminal MySQL:" && docker-compose exec mysql sh

bash_redis:
	@echo "Terminal Redis:" && docker-compose exec redis sh

# composer:
# 	@echo "Composer Laravel...." && docker-compose exec laravel.test php composer

